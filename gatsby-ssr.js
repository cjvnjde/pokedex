exports.onPreRouteUpdate = ({ location, prevLocation }) => {
  console.log('SSR Gatsby started to change location to', location.pathname);
  console.log('SSR Gatsby started to change location from', prevLocation ? prevLocation.pathname : null);
};


exports.onRenderBody = ({ setBodyAttributes }) => {
  setBodyAttributes({
    className: 'body',
  });
};
