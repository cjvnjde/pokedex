const fetch = require('node-fetch');
const queryString = require('query-string');

const rootApiUrl = 'https://pokeapi.co/api/v2/';

function fetchPokemonList(configOptions) {
  const apiOptions = queryString.stringify(configOptions);
  const apiUrl = `${rootApiUrl}pokemon?${apiOptions}`;

  return fetch(apiUrl)
    .then(response => response.json())
    .then(data => data.results.map(({ name, url }) => ({ name, id: Number(/https:\/\/pokeapi\.co\/api\/v2\/pokemon\/(\d*)/.exec(url)[1]) })));
}

function fetchPokemon(pokemonId) {
  const apiUrl = `${rootApiUrl}pokemon/${pokemonId}`;

  return fetch(apiUrl)
    .then(response => response.json())
    .then(data => ({
      abilities: data.abilities.map(({ ability }) => ability.name),
      baseExperience: data.base_experience,
      forms: data.forms.map(({ name }) => name),
      height: data.height,
      id: data.id,
      name: data.name,
      pokemonId: data.id,
      // eslint-disable-next-line camelcase
      stats: data.stats.map(({ stat, base_stat, effort }) => ({
        name: stat.name,
        value: base_stat,
        effort,
      })),
      types: data.types.map(({ type }) => type.name),
      weight: data.weight,
    }));
}

exports.sourceNodes = async (
  { actions, createNodeId, createContentDigest },
  configOptions,
) => {
  const localeConfigOptions = { ...configOptions };
  delete localeConfigOptions.plugins;
  const { createNode } = actions;

  const processPokemon = pokemon => {
    const nodeId = createNodeId(`pokemon-${pokemon.id}`);
    const nodeContent = JSON.stringify(pokemon);
    return {
      ...pokemon,
      id: nodeId,
      parent: null,
      children: [],
      internal: {
        type: 'Pokemon',
        content: nodeContent,
        contentDigest: createContentDigest(pokemon),
      },
    };
  };

  const pokemonList = await fetchPokemonList(localeConfigOptions);
  const pokemons = pokemonList.map(({ id }) => fetchPokemon(id));

  await Promise.all(pokemons).then(data => {
    data.forEach(pokemon => {
      const nodeData = processPokemon(pokemon);
      createNode(nodeData);
    });
  });
};
