const path = require('path');

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions;

  const result = await graphql(
    `
      {
        allPokemon(
          limit: 1000
        ) {
          edges {
            node {
              id
              name
            }
          }
        }
      }
    `,
  );

  if (result.errors) {
    reporter.panicOnBuild('Error while running GraphQL query.');
    return;
  }

  const pokemons = result.data.allPokemon.edges;
  const pokemonsPerPage = 6;
  const numPages = Math.ceil(pokemons.length / pokemonsPerPage);

  Array.from({ length: numPages }).forEach((_, i) => {
    createPage({
      path: i === 0 ? '/' : `/${i + 1}`,
      component: path.resolve('./src/templates/pokemon-list-template.jsx'),
      context: {
        limit: pokemonsPerPage,
        skip: i * pokemonsPerPage,
        numPages,
        currentPage: i + 1,
      },
    });
  });

  pokemons.forEach(({ node }) => {
    createPage({
      path: `/${node.name}`,
      component: path.resolve('./src/templates/pokemon-template.jsx'),
      context: {
        name: node.name,
      },
    });
  });

  const markdowns = await graphql(`
    {
      allMarkdownRemark(
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `);

  if (markdowns.errors) {
    reporter.panicOnBuild('Error while running GraphQL query.');
    return;
  }

  markdowns.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.frontmatter.path,
      component: path.resolve('src/templates/about-template.jsx'),
    });
  });
};

exports.onCreateNode = ({
  node,
  actions,
  getNode,
  getNodes,
}) => {
  const { createNodeField } = actions;

  if (node.internal.type === 'Pokemon') {
    const imageNode = getNodes()
      // eslint-disable-next-line no-shadow
      .filter(imageNode => imageNode.internal.owner === 'gatsby-transformer-sharp')
      // eslint-disable-next-line no-shadow
      .find(imageNode => {
        const parentNode = getNode(imageNode.parent);

        return parentNode.name === String(node.pokemonId);
      });

    createNodeField({
      name: 'image___NODE',
      node,
      value: imageNode.id,
    });
  }
};
