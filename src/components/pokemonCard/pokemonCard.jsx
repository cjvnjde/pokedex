import PropTypes from 'prop-types';
import React from 'react';
import Img from 'gatsby-image';
import { Link } from 'gatsby';

import './pokemonCardStyle.scss';

const PokemonCard = ({ fluid, name }) => (
  <Link
    className="pokemon-card"
    to={`/${name}`}
  >
    <div className="pokemon-card__image-wrapper">
      <Img fluid={fluid} />
    </div>
    {name}
  </Link>
);

PokemonCard.propTypes = {
  fluid: PropTypes.shape({
    aspectRatio: PropTypes.number.isRequired,
    base64: PropTypes.string.isRequired,
    sizes: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    srcSet: PropTypes.string.isRequired,
  }).isRequired,
  name: PropTypes.string.isRequired,
};


export default PokemonCard;
