import React from 'react';

import './loaderStyle.scss';

const Loader = () => <div className="loader" />;

export default Loader;
