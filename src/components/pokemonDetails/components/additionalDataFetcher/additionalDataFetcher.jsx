import PropTypes from 'prop-types';
import React, { useState } from 'react';

import './additionalDataFetcherStyle.scss';
import Loader from '../../../loader';

const AdditionalDataFetcher = ({ name }) => {
  const [isLoading, changeLoading] = useState(false);
  const [isLoaded, changeLoaded] = useState(false);
  const [data, changeData] = useState({});

  if (isLoading) {
    return <Loader />;
  }

  const loadMore = () => {
    changeLoading(true);

    fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
      .then(response => response.json())
      // eslint-disable-next-line no-shadow
      .then(data => {
        changeData(data);
        changeLoaded(true);
      })
      .catch(() => {
        changeLoaded(false);
      })
      .finally(() => {
        changeLoading(false);
      });
  };

  if (!isLoaded) {
    return (
      <div
        className="pokemon-data-fetcher__load-more"
        role="button"
        tabIndex="0"
        onKeyPress={() => loadMore()}
        onClick={() => loadMore()}
      >
        Load Mode
      </div>
    );
  }

  return (
    <div className="pokemon-data-fetcher">
      Moves:
      <div>
        {
          data.moves.map(({ move }) => move.name).join(', ')
        }
      </div>
    </div>
  );
};

AdditionalDataFetcher.propTypes = {
  name: PropTypes.string.isRequired,
};


export default AdditionalDataFetcher;
