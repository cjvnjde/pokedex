import PropTypes from 'prop-types';
import React from 'react';
import Img from 'gatsby-image';

import './pokemonDetailsStyle.scss';
import AdditionalDataFetcher from './components/additionalDataFetcher';

const PokemonDetails = ({ fluid, name, pokemon }) => (
  <div className="pokemon-details">
    {name}
    <div className="pokemon-details__image-wrapper">
      <Img fluid={fluid} />
    </div>
    <table>
      <tbody>
        <tr>
          <td>
            Height
          </td>
          <td>
            {pokemon.height}
          </td>
        </tr>
        <tr>
          <td>
            Weight
          </td>
          <td>
            {pokemon.weight}
          </td>
        </tr>
        <tr>
          <td>
            Base Experience
          </td>
          <td>
            {pokemon.baseExperience}
          </td>
        </tr>
        <tr>
          <td>
            Abilities
          </td>
          <td>
            {pokemon.abilities.map(ability => (
              <div key={ability}>
                {ability}
              </div>
            )) || '-'}
          </td>
        </tr>
        <tr>
          <td>
            Types
          </td>
          <td>
            {pokemon.types.map(type => (
              <div key={type}>
                {type}
              </div>
            )) || '-'}
          </td>
        </tr>
        <tr>
          <td>
            Stats
          </td>
          <td>
            {pokemon.stats.map(({
              // eslint-disable-next-line no-shadow
              name,
              effort,
              value,
            }) => (
              <tr key={name}>
                <td>
                  {name}
                </td>
                <td>
                  {value}
                </td>
                <td>
                  +
                  {effort}
                </td>
              </tr>
            )) || '-'}
          </td>
        </tr>
      </tbody>
    </table>
    <AdditionalDataFetcher name={name} />
  </div>
);

PokemonDetails.propTypes = {
  fluid: PropTypes.shape({
    aspectRatio: PropTypes.number.isRequired,
    base64: PropTypes.string.isRequired,
    sizes: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    srcSet: PropTypes.string.isRequired,
  }).isRequired,
  name: PropTypes.string.isRequired,
  pokemon: PropTypes.shape({
    abilities: PropTypes.arrayOf(PropTypes.string).isRequired,
    baseExperience: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    stats: PropTypes.arrayOf(PropTypes.shape({
      effort: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      value: PropTypes.number.isRequired,
    })).isRequired,
    types: PropTypes.arrayOf(PropTypes.string).isRequired,
    weight: PropTypes.number.isRequired,
  }).isRequired,
};


export default PokemonDetails;
