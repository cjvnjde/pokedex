import PropTypes from 'prop-types';
import React from 'react';

import './pokemonCardWrapperStyle.scss';

const PokemonCardWrapper = ({ children }) => (
  <div className="pokemon-card-wrapper">
    {children}
  </div>
);

PokemonCardWrapper.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
};

export default PokemonCardWrapper;
