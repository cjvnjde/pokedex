import PropTypes from 'prop-types';
import React from 'react';

import './paginationStyle.scss';
import PaginationCard from './components/paginationCard';

const Pagination = ({
  totalPages,
  currentPage,
  createRoute,
  limit,
}) => {
  function renderMiddlePart() {
    if (currentPage < limit) {
      return [...Array.from({ length: limit - 1 }).map((_, index) => {
        if (index + 2 === currentPage) {
          return (
            <PaginationCard
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              active
              to={createRoute(index + 2)}
            >
              {index + 2}
            </PaginationCard>
          );
        }

        return (
          <PaginationCard
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            to={createRoute(index + 2)}
          >
            {index + 2}
          </PaginationCard>
        );
      }), <PaginationCard>...</PaginationCard>];
    }

    if (currentPage > totalPages - limit + 1) {
      return [<PaginationCard>...</PaginationCard>,
        ...Array.from({ length: limit - 1 }).map((_, index) => {
          if (index + totalPages - limit + 1 === currentPage) {
            return (
              <PaginationCard
                // eslint-disable-next-line react/no-array-index-key
                key={index}
                active
                to={createRoute(index + totalPages - limit + 1)}
              >
                {index + totalPages - limit + 1 }
              </PaginationCard>
            );
          }

          return (
            <PaginationCard
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              to={createRoute(index + totalPages - limit + 1)}
            >
              {index + totalPages - limit + 1}
            </PaginationCard>
          );
        }),
      ];
    }

    return [
      currentPage - limit > 0 ? <PaginationCard>...</PaginationCard> : null,
      <PaginationCard to={createRoute(currentPage - 1)}>
        {currentPage - 1}
      </PaginationCard>,
      <PaginationCard active to={createRoute(currentPage)}>
        {currentPage}
      </PaginationCard>,
      <PaginationCard to={createRoute(currentPage + 1)}>
        {currentPage + 1}
      </PaginationCard>,
      totalPages - currentPage - limit + 1 > 0 ? <PaginationCard>...</PaginationCard> : null,
    ].filter(t => t);
  }

  if (totalPages > limit + 2) {
    return (
      <div className="pagination">
        <PaginationCard active={currentPage === 1} to={createRoute(1)}>
          1
        </PaginationCard>
        {renderMiddlePart()}
        <PaginationCard active={currentPage === totalPages} to={createRoute(totalPages)}>
          {totalPages}
        </PaginationCard>
      </div>
    );
  }

  return (
    <div className="pagination">
      {Array.from({ length: totalPages }).map((_, index) => {
        if (index + 1 === currentPage) {
          return (
            <PaginationCard
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              active
              to={createRoute(index + 1)}
            >
              {index + 1}
            </PaginationCard>
          );
        }

        return (
          <PaginationCard
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            to={createRoute(index + 1)}
          >
            {index + 1}
          </PaginationCard>
        );
      })}
    </div>
  );
};

Pagination.propTypes = {
  totalPages: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  createRoute: PropTypes.func.isRequired,
  limit: PropTypes.number,
};

Pagination.defaultProps = {
  limit: 3,
};

export default Pagination;
