import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'gatsby';

import './paginationCardStyle.scss';

const PaginationCard = ({ children, active, to }) => {
  if (!to) {
    return (
      <div className="pagination-card pagination-card--disabled">
        {children}
      </div>
    );
  }

  if (!active) {
    return (
      <Link
        className="pagination-card"
        to={to}
      >
        {children}
      </Link>
    );
  }

  return (
    <div className="pagination-card pagination-card--active">
      {children}
    </div>
  );
};

PaginationCard.propTypes = {
  children: PropTypes.node.isRequired,
  active: PropTypes.bool,
  to: PropTypes.string,
};

PaginationCard.defaultProps = {
  active: false,
  to: '',
};

export default PaginationCard;
