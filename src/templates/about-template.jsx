import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';

import Seo from '../components/seo';
import Layout from '../components/layout';

const AboutTemplate = ({
  data,
}) => {
  const { markdownRemark } = data;
  const { frontmatter } = markdownRemark;

  return (
    <Layout>
      <Seo title="Home" />
      <h1>{frontmatter.title}</h1>
      <h5>{frontmatter.description}</h5>
    </Layout>
  );
};

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      frontmatter {
        title
        description
      }
    }
  }
`;

AboutTemplate.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.shape({
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};

export default AboutTemplate;
