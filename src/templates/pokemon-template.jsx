import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';

import Layout from '../components/layout';
import Seo from '../components/seo';
import PokemonDetails from '../components/pokemonDetails';

const Pokemon = ({ data }) => {
  const { pokemon } = data;

  return (
    <Layout>
      <Seo title={pokemon.name} />
      <PokemonDetails
        fluid={pokemon.fields.image.fluid}
        name={pokemon.name}
        pokemon={pokemon}
      />
    </Layout>
  );
};

export const pokemonQuery = graphql`
  query pokemonQuery($name: String!) {
    pokemon(
      name: {eq: $name}
    ) {
      id
      name
      height
      weight
      baseExperience
      abilities
      stats {
        effort
        name
        value
      }
      types
      fields {
        image {
          fluid(maxWidth: 600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`;

Pokemon.propTypes = {
  data: PropTypes.shape({
    pokemon: PropTypes.shape({
      name: PropTypes.string.isRequired,
      fields: PropTypes.shape({
        image: PropTypes.shape({
          fluid: PropTypes.shape({
            aspectRatio: PropTypes.number.isRequired,
            base64: PropTypes.string.isRequired,
            sizes: PropTypes.string.isRequired,
            src: PropTypes.string.isRequired,
            srcSet: PropTypes.string.isRequired,
          }).isRequired,
        }).isRequired,
      }).isRequired,
      abilities: PropTypes.arrayOf(PropTypes.string).isRequired,
      baseExperience: PropTypes.number.isRequired,
      height: PropTypes.number.isRequired,
      stats: PropTypes.arrayOf(PropTypes.shape({
        effort: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired,
      })).isRequired,
      types: PropTypes.arrayOf(PropTypes.string).isRequired,
      weight: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
};

export default Pokemon;
