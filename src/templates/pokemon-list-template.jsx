import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';

import Layout from '../components/layout';
import Seo from '../components/seo';
import PokemonCard from '../components/pokemonCard';
import PokemonCardWrapper from '../components/pokemonCardWrapper';
import Pagination from '../components/pagination';

function createRoute(pageNumber) {
  if (pageNumber === 1) {
    return '/';
  }

  return `/${pageNumber}`;
}

const PokemonList = ({ data, pageContext }) => {
  const pokemons = data.allPokemon.edges;
  const { currentPage, numPages } = pageContext;

  return (
    <Layout>
      <Seo title={currentPage === 1 ? 'pokedex' : `pokedex page ${currentPage}`} />
      <Pagination
        createRoute={createRoute}
        currentPage={currentPage}
        totalPages={numPages}
      />
      <PokemonCardWrapper>
        {pokemons.map(({ node }) => {
          const { name } = node;
          const { fluid } = node.fields.image;

          return (
            <PokemonCard key={node.id} fluid={fluid} name={name} />
          );
        })}
      </PokemonCardWrapper>
      <Pagination
        createRoute={createRoute}
        currentPage={currentPage}
        totalPages={numPages}
      />
    </Layout>
  );
};

export const pokemonListQuery = graphql`
  query pokemonListQuery($skip: Int!, $limit: Int!) {
    allPokemon(
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          id
          name
          fields {
            image {
              fluid(maxWidth: 300) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`;

PokemonList.propTypes = {
  data: PropTypes.shape({
    allPokemon: PropTypes.shape({
      edges: PropTypes.arrayOf(PropTypes.shape({
        node: PropTypes.shape({
          name: PropTypes.string.isRequired,
          fields: PropTypes.shape({
            image: PropTypes.shape({
              fluid: PropTypes.shape({
                aspectRatio: PropTypes.number.isRequired,
                base64: PropTypes.string.isRequired,
                sizes: PropTypes.string.isRequired,
                src: PropTypes.string.isRequired,
                srcSet: PropTypes.string.isRequired,
              }).isRequired,
            }).isRequired,
          }).isRequired,
        }).isRequired,
      })).isRequired,
    }).isRequired,
  }).isRequired,
  pageContext: PropTypes.shape({
    currentPage: PropTypes.number.isRequired,
    numPages: PropTypes.number.isRequired,
  }).isRequired,
};

export default PokemonList;
